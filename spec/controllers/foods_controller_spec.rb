require 'rails_helper'

RSpec.describe FoodsController, :type => :controller do


  describe 'GET #index' do
    it 'populates an array of recipes' do
      test_food = create(:food)
      get :index
      expect(assigns(:foods)).to eq([test_food])
    end

    it 'renders the :index view' do
      get :index
      expect(response).to render_template :index
    end
  end

  describe 'GET #show' do
    before(:each) do
      @test_food = create(:food)
    end

    it 'assigns the pertinent Food to @food' do
      get :show, id: @test_food
      expect(assigns(:food)).to eq(@test_food)
    end

    it 'renders the #show view' do
      get :show, id: @test_food
      expect(response).to render_template :show
    end
  end

  describe 'GET #new' do
    it 'renders the #new view' do
      get :new
      expect(response).to render_template :new
    end
  end

  describe 'GET #edit' do
    before(:each) do
      @test_food = create(:food)
    end

    it 'assigns the pertinent Food to @food' do
      get :edit, id: @test_food
      expect(assigns(:food)).to eq(@test_food)
    end

    it 'renders the #show view' do
      get :edit, id: @test_food
      expect(response).to render_template :edit
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      before(:each) do
        post :create, food: attributes_for(:food, :chicken_soup)
      end

      it 'creates the food recipe' do
        expect(Food.count).to eq(1)
      end

      it 'redirects to the new recipe' do
        expect(response).to redirect_to Food.last
      end
    end

    context 'with invalid attributes' do
      before(:each) do
        post :create, food: attributes_for(:food, :invalid)
      end

      it 'creates the food recipe' do
        expect(Food.count).to eq(0)
      end

      it 're-renders the new method form page' do
        expect(response).to render_template :new
      end
    end
  end

  describe 'PUT #update' do
    before(:each) do
      @test_food = create(:food)
    end

    context "valid attributes" do
      it 'located the correct Food' do
        put :update, id: @test_food, food: attributes_for(:food)
        expect(assigns(:food)).to eq(@test_food)
      end

      it 'changes the Food attributes' do
        put :update, id: @test_food, food: attributes_for(:food, :plain_soup)
        @test_food.reload
        expect(@test_food.name).to eq("Plain Soup")
        expect(@test_food.haley_comment).to eq("Meh.")
        expect(@test_food.tags).to eq("Soup")
      end

      it 'redirects to the updated Food' do
        put :update, id: @test_food, food: attributes_for(:food)
        expect(response).to redirect_to @test_food
      end
    end
  end

  describe 'DELETE #destroy' do
    before(:each) do
      @test_food = create(:food)
    end

    it 'destroys the selected food' do
      expect {
        delete :destroy, id: @test_food
      }.to change(Food, :count).by(-1)
    end

    it 'redirects the user to the foods index' do
      delete :destroy, id: @test_food
      expect(response).to redirect_to :foods
    end
  end
end
