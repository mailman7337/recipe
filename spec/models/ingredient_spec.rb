require 'rails_helper'

RSpec.describe Ingredient, :type => :model do
  subject { create(:food, :chicken_soup).ingredients[0] }
  
  it 'has a valid factory' do
    expect(build(:ingredient)).to be_valid
  end

  it 'is invalid without a name' do
    expect(build(:ingredient, name: nil)).to_not be_valid
  end

  it 'is invalid without a quantity' do
    expect(build(:ingredient, quantity: nil)).to_not be_valid
  end
end
