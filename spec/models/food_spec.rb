require 'rails_helper'

RSpec.describe Food, :type => :model do
  subject { create(:food) }
  let(:chicken_soup) { create(:food,:chicken_soup) }
  let(:food_with_no_tags) { create(:food, tags: "") }
  let(:food_with_nil_tags) { create(:food, tags: nil) }
  let(:burger_with_cheese) { create(:food, :burger_with_cheese) }

  it 'has a valid factory' do
    expect(build(:food)).to be_valid
  end

  it 'is invalid without a name' do
    expect(build(:food, name: nil)).to_not be_valid
  end

  describe '.matching' do
    pending('all the test cases')
  end

  describe '#tag_array' do
    it 'returns an empty array if the food has no tags' do
      expect(food_with_no_tags.tag_array).to eql([])
    end

    it 'returns an empty array if the food\'s tags are nil' do
      expect(food_with_nil_tags.tag_array).to eql([])
    end

    it 'returns an array with 1 entry per tag' do
      expect(subject.tag_array.size).to eql(4)
    end

    it 'returns the correct string entries in the array' do
      expect(subject.tag_array[0]).to eql('Chicken')
    end

    it 'correctly removes leading and trailing spaces from entries' do
      expect(subject.tag_array[1]).to eql('Soup')
    end
  end

  describe '#can_make_without?' do
    it 'returns false if its ingredients include blacklist items' do
      expect(burger_with_cheese.can_make_without?( %w( Cheese Licorice ) )).to eq(false)
    end

    it 'returns false if its ingredients include blacklist items of a different cASe' do
      expect(burger_with_cheese.can_make_without?( %w( ChEeSe Licorice ) )).to eq(false)
    end

    it 'returns true if its ingredients don\'t include blacklist items' do
      expect(burger_with_cheese.can_make_without?( %w( Licorice Tobasco ) )).to eq(true)
    end

    it 'returns true if the blacklist is nil' do
      expect(burger_with_cheese.can_make_without?( nil )).to eq(true)
    end

    it 'returns true if the blacklist is an empty array' do
      expect(burger_with_cheese.can_make_without?( [] )).to eq(true)
    end

    it 'returns true if the ingredients list is empty' do
      expect(subject.can_make_without?( %w( Cheese Licorice ) )).to eq(true)
    end
  end

  describe '#passes_tags_blacklist?' do
    it 'returns false if its tags include blacklist items' do
      expect(subject.passes_tags_blacklist?( ["Broth"] )).to eq(false)
    end

    it 'returns false if its tags include blacklist items of a different cASe' do
      expect(subject.passes_tags_blacklist?( ["BrOTh"] )).to eq(false)
    end

    it 'returns true if its tags don\'t include blacklist items' do
      expect(subject.passes_tags_blacklist?( ["Kimchi", "Yogurt"] )).to eq(true)
    end

    it 'returns true if the blacklist is nil' do
      expect(subject.passes_tags_blacklist?(nil)).to eq(true)
    end

    it 'returns true if the blacklist is an empty array' do
      expect(subject.passes_tags_blacklist?([])).to eq(true)
    end

    it 'returns true if the tags string is empty' do
      expect(food_with_no_tags.passes_tags_blacklist?( ["Broth"] )).to eq(true)
    end

    it 'returns true if the tags string is nil' do
      expect(food_with_nil_tags.passes_tags_blacklist?( ["Broth"] )).to eq(true)
    end
  end

  describe '#passes_blacklist?' do 
    it 'returns true if blacklist is nil' do
      expect(subject.passes_blacklist?(nil)).to eq(true)
    end

    it 'returns true if blacklist is empty' do
      expect(subject.passes_blacklist?([])).to eq(true)
    end

    it 'returns true if neither ingredients nor tags include a blacklist item' do
      expect(chicken_soup.passes_blacklist?(["Pizza"])).to eq(true)
    end

    it 'returns false if ingredients have a blacklist item and tags do not' do
      expect(burger_with_cheese.passes_blacklist?(["Cheese"])).to eq(false)
    end

    it 'returns false if tags have a blacklist item and ingredients do not' do
      expect(subject.passes_blacklist?(["Broth"])).to eq(false)
    end

    it 'returns false if ingredients and tags both have a blacklist item' do 
      expect(chicken_soup.passes_blacklist?(["Chicken"])).to eq(false)
    end
  end

  describe '#matches_ingredient?' do
    it 'returns false if its ingredients don\'t include the search item' do
      expect(chicken_soup.matches_ingredient?( "Salt" )).to eq(false)
    end

    it 'returns true if its ingredients include search item' do
      expect(chicken_soup.matches_ingredient?("Broth")).to eq(true)
    end

    it 'returns true if its ingredients include search items of a different cASe' do
      expect(chicken_soup.matches_ingredient?("BrOTh")).to eq(true)
    end

    it 'returns true if the search item is nil' do
      expect(chicken_soup.matches_ingredient?(nil)).to eq(true)
    end

    it 'returns true if the search item is empty' do
      expect(chicken_soup.matches_ingredient?("")).to eq(true)
    end

    it 'returns false if the ingredients list is empty and the search item is not' do
      expect(subject.matches_ingredient?( "Salt" )).to eq(false)
    end
  end

  describe '#matches_tags?' do
    it 'returns false if its tags don\'t include the search item' do
      expect(subject.matches_tags?( "Dessert" )).to eq(false)
    end

    it 'returns true if its tags include search item' do
      expect(chicken_soup.matches_tags?("Broth")).to eq(true)
    end

    it 'returns true if its tags include search items of a different cASe' do
      expect(chicken_soup.matches_tags?("BrOTh")).to eq(true)
    end

    it 'returns true if the search item is nil' do
      expect(chicken_soup.matches_tags?(nil)).to eq(true)
    end

    it 'returns true if the search item is empty' do
      expect(chicken_soup.matches_tags?("")).to eq(true)
    end

    it 'returns false if the tags list is empty and the search item is not' do
      expect(subject.matches_tags?( "Salt" )).to eq(false)
    end
  end

  describe '#matches_tags_or_ingredients?' do
    it 'returns true if the search items list is nil' do
      expect(subject.matches_tags_or_ingredients?(nil)).to eq(true)
    end

    it 'returns true if the search items list is empty' do
      expect(subject.matches_tags_or_ingredients?([])).to eq(true)
    end

    it 'returns false if an incomplete set of search terms are matched' do
      expect(chicken_soup.matches_tags_or_ingredients?( %w( Broth Pizza ) )).to eq(false)
    end

    it 'returns true if all search terms are matched in only ingredients (not tags)' do
      expect(chicken_soup.matches_tags_or_ingredients?( %w( Noodles herbs ) )).to eq(true)
    end

    it 'returns true if all search terms are matched in only tags (not ingredients)' do
      expect(chicken_soup.matches_tags_or_ingredients?( %w( Soup dinner ) )).to eq(true)
    end

    it 'returns true if all search terms are matched in both tags and ingredients' do
      expect(chicken_soup.matches_tags_or_ingredients?( %w( Chicken Broth ) )).to eq(true)
    end
  end
end