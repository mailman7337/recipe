FactoryGirl.define do
  factory :food do
    name   "Burgers"
    author "Haley"
    source "http://smittenkitchen.com"
    haley_comment "It's good"
    tags "Chicken, Soup, Broth, Dinner"
    content "Step 1, Step 2, Step 3"

    trait :plain_soup do
      name  "Plain Soup"
      haley_comment "Meh."
      tags "Soup"

      after(:build) do |plain_soup|
        plain_soup.ingredients << FactoryGirl.build(:ingredient, food: plain_soup )
      end
    end

    trait :chicken_soup do 
      name "Chicken Noodle Soup"
      tags "Chicken, Soup, Broth, Dinner"
      haley_comment "It's great!"

      after(:build) do |chicken_soup|
        chicken_soup.ingredients << FactoryGirl.build(:ingredient, food: chicken_soup, name: "Chicken" )
        chicken_soup.ingredients << FactoryGirl.build(:ingredient, food: chicken_soup, name: "Noodles" )
        chicken_soup.ingredients << FactoryGirl.build(:ingredient, food: chicken_soup, name: "Chicken Broth" )
        chicken_soup.ingredients << FactoryGirl.build(:ingredient, food: chicken_soup, name: "Herbs" )
      end
    end

    trait :burger_with_cheese do 
      after(:build) do |burger_with_cheese|
        burger_with_cheese.ingredients << FactoryGirl.build(:ingredient, food: burger_with_cheese, name: "Cheese" )
      end
    end

    trait :invalid do
      name nil
    end
  end
end
