FactoryGirl.define do
  factory :ingredient do
    name     "Sugar"
    quantity 1
    metric   "Cup"
  end
end
