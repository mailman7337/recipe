class CreateFoodsAndIngredients < ActiveRecord::Migration
  def change
    create_table :foods do |t|
      t.string :name
      t.string :author
      t.string :source
      t.string :haley_comment
      t.string :tags
      t.text :content

      t.timestamps null: false
    end

    create_table :ingredients do |t|
      t.belongs_to :food, index: true
      t.string :name
      t.string :quantity
      t.string :metric

      t.timestamps null: false
    end

    # -- NOTE FOR FUTURE DEVELOPMENT
    # 
    # Matching the note in app/models/food.rb, this table would support
    # the user being able to track when they made the recipe, along with
    # how awesome it was each time.
    #
    #   create_table :occasions do |t|
    #     t.belongs_to :food, index: true
    #     t.integer :awesomeness
    #     t.date :date_made
    #   end
  end
end
