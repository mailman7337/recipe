class Ingredient < ActiveRecord::Base
  belongs_to :food

  validates :name, presence: true
  validates :quantity, presence: true

  def metrics 
    %w{
      bunch
      cup
      fl-oz
      gallon
      gram
      lb
      liter
      ml
      oz
      pinch
      pint
      quart
      tbsp
      tsp
    }
  end

end
