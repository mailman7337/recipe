class Food < ActiveRecord::Base
  has_many :ingredients, dependent: :destroy

  # Ingredients ignored unless they have both a name and a quantity
  accepts_nested_attributes_for :ingredients, 
    reject_if: proc { |ingredient| ingredient[:name].blank? || ingredient[:quantity].blank? },
    allow_destroy: true

  validates :name, presence: true

  def self.matching( search_string )
    return Food.all.order(created_at: :desc).select do |f| 
      search_array = search_string.split(',').map(&:strip)
      whitelist = search_array.select { |elem| elem =~ /\A[^\-].*/ } # only elements that don't start with a dash
      blacklist = search_array.select { |elem| elem =~ /\A\-.*/ } # only elements that start with a dash

      blacklist.map { |elem| elem.slice!(0) } # remove leading dash on each item

      f.matches_tags_or_ingredients?(whitelist) && f.passes_blacklist?(blacklist)
    end
  end

  def tag_array
    return [] if tags.nil? || tags.empty?
    tags.split(',').map(&:strip) # remove leading & trailing spaces after split
  end


  def can_make_without?( missing_ingredients )
    return true if missing_ingredients.nil?

    # Ensure that wrong case doesn't prevent a match
    lowercase_missing_ingredients = missing_ingredients.map(&:downcase)

    ingredients.each do |ingredient|
      return false if lowercase_missing_ingredients.include? ingredient.name.downcase
    end
    true # Recipe doesn't use the missing ingredients 
  end


  def passes_tags_blacklist?( blacklist )
    return true if blacklist.nil? || tags.nil?

    # Ensure that wrong case doesn't prevent a match
    lowercase_blacklist = blacklist.map(&:downcase)
    tag_array.each do |tag|
      return false if lowercase_blacklist.include? tag.downcase
    end
    true # Recipe tags don't include blacklist items
  end


  def passes_blacklist?( blacklist )
    can_make_without?( blacklist ) && passes_tags_blacklist?( blacklist )
  end


  def matches_ingredient?( search_ingredient )
    return true if search_ingredient.nil? || search_ingredient.empty?

    ingredients.each do |ingredient|
      return true if ingredient.name.downcase.include? search_ingredient.downcase
    end
    false # Recipe ingredients don't include the search term
  end


  def matches_tags?( search )
    return true if search.nil? || search.empty?

    tag_array.each do |tag|
      return true if tag.downcase.include? search.downcase
    end
    false # Recipe tags don't include the search term
  end


  def matches_tags_or_ingredients?( search_array )
    return true if search_array.nil? || search_array.empty?

    search_array.each do |search|
      return false unless matches_ingredient?(search) || matches_tags?(search)
    end
    true # All search terms were matched
  end



  #  -- NOTE FOR FUTURE FUNCTIONALITY
  # 
  # In future revisions of the recipe tracker, the user will be able to
  # record when they have made the recipe, and record how awesome the 
  # recipe was (on a scale of 1-5). The user would then be able to sort
  # retrieved recipes by when it was last made, by total achieved awesome-
  # ness, or by average awesomeness.
  #
  #    def last_made
  #      # Not done
  #    end
  #
  #    def number_of_times_made
  #      # Not done
  #    end
  #
  #    def average_awesomeness
  #      # Not done
  #    end 
end
