class FoodsController < ApplicationController
  def index
    if params[:search] 
      @foods = Food.matching( params[:search] )
      @search = params[:search]
    else 
      @foods = Food.all.order(created_at: :desc)
    end
  end
  
  def new
    @food = Food.new
  end

  def show
    @food = Food.find(params[:id])
  end

  def edit
    @food = Food.find(params[:id])
  end

  def create 
    @food = Food.new( food_params )

    respond_to do |format|
      if @food.save
        format.html { redirect_to @food, notice: 'Recipe was successfully created.' }
      else
        format.html { render 'new' }
      end
    end
  end

  def update 
    @food = Food.find( params[:id] )

    respond_to do |format|
      if @food.update!( food_params )
        format.html { redirect_to @food, notice: 'Recipe was successfully updated.' }
      else
        format.html { render 'edit' }
      end
    end
  end

  def destroy
    @food = Food.find( params[:id] )

    respond_to do |format|
      if @food.destroy
        format.html { redirect_to foods_url, notice: 'Recipe was successfully updated.' }
      else
        format.html { redirect_to @food, alert: 'Unable to delete food.' }
      end
    end 
  end

  private
  # Using a private method to encapsulate the permissible parameters is just a good pattern
  # since you'll be able to reuse the same permit list between create and update. Also, you
  # can specialize this method with per-user checking of permissible attributes.
  def food_params
    params.require(:food).permit(:name, :author, :source, :haley_comment, :tags, :content, ingredients_attributes: [ :id, :name, :quantity, :metric, :_destroy ] )
  end
end
